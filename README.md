# dockerized-php-env



## Getting started

Clone the project  
  
To create containers of your future project (you can/must change containers name in docker-compose file and futur project path in vhosts.conf file):   
```
cd dockerized-php-env  
docker-compose up -d
```

URL : 
- PhpMyAdmin : http://127.0.0.1:8081
- MailDev : http://127.0.0.1:8082
- Web : http://127.0.0.1:8080
  
To connect to containers :
`docker exec -it <CONTAINER_ID> bash`

